var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());
//function that prints a tabular form of the user list
function printTable(message, userList) {
    var str = "";
    str += "<div id='serverMessage'> " + message + "</div>\n";
    //creation of table if list not empty
    if (userList.length > 0) {
        str += "<div id='userList'>\n";
        str += "  <table>\n";
        str += "    <tr>\n";
        str += "      <th> ID       </th>\n";
        str += "      <th> First Name  </th>\n";
        str += "      <th> Last Name </th>\n";
        str += "      <th> Email </th>\n";
        str += "      <th> Role </th>\n";
        str += "    </tr>\n";
        for (var _i = 0, userList_1 = userList; _i < userList_1.length; _i++) {
            var user = userList_1[_i];
            if (user != null) {
                str += "    <tr " + user._id + ">\n";
                str += "      <td> " + user._id + " </td>\n";
                str += "      <td> " + user.firstName + " </td>\n";
                str += "      <td> " + user.lastName + " </td>\n";
                str += "      <td> " + user.email + " </td>\n";
                str += "      <td> " + user.role + " </td>\n";
                str += "    </tr>\n";
            }
        }
    }
    str += "  </table>\n";
    str += "</div>";
    $('#tab').html(str); //add string to the html file at div tab
}
$(function () {
    //creation of user
    $('#add').on("click", function () {
        var vorname = $('#firstnameInput').val().trim();
        var nachname = $('#lastnameInput').val().trim();
        var mail = $('#emailInput').val().trim();
        var pass = $('#passwordInput').val().trim();
        var rolle = $("#roleChoice").val().trim();
        var data = { firstName: vorname, lastName: nachname, email: mail, password: pass, role: rolle }; //data to be sent to server
        $.ajax({
            url: 'http://localhost:8000/user',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) { printTable(data.message, data.userList); },
            error: function (jqXHR) { printTable(jqXHR.responseJSON.message, []); }
        });
    });
    //click on button edit sends request from client to server with id of user to be changed and object data that contains fields to be changed
    $('#edit').on("click", function () {
        var vorname = $('#firstnameInput').val().trim();
        var nachname = $('#lastnameInput').val().trim();
        var mail = $('#emailInput').val().trim();
        var pass = $('#passwordInput').val().trim();
        var rolle = $('#roleChoice').val().trim();
        var id = $('#userIDInput').val();
        var data = { firstName: vorname, lastName: nachname, email: mail, password: pass, role: rolle }; //data to be sent to server
        $.ajax({
            url: 'http://localhost:8000/user/' + id,
            type: 'PUT',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) { printTable(data.message, data.userList); },
            error: function (jqXHR) { printTable(jqXHR.responseJSON.message, []); }
        });
    });
    //click on button delete sends request from client to server with id of user to be deleted
    $('#delete').on("click", function () {
        var id = $('#userIDInput').val();
        $.ajax({
            url: 'http://localhost:8000/user/' + id,
            type: 'DELETE',
            dataType: 'json',
            success: function (data) { printTable(data.message, data.userList); },
            error: function (jqXHR) { printTable(jqXHR.responseJSON.message, []); }
        });
    });
    //click on button search sends request from client to server
    $('#search').on("click", function () {
        var name = $('#searchInput').val();
        $.ajax({
            url: 'http://localhost:8000/users/searchname/' + name,
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) { printTable(data.message, data.userList); },
            error: function (jqXHR) { printTable(jqXHR.responseJSON.message, []); } //if error sends message error
        });
    });
    //--- click on the read list button ----------------------------------------
    $('#readByID').on("click", function () {
        $.ajax({
            url: 'http://localhost:8000/users',
            type: 'GET',
            dataType: 'json',
            success: function (data) { printTable(data.message, data.userList); },
            error: function (jqXHR) { printTable(jqXHR.responseJSON.message, []); }
        });
    });
    //--- click on the list by firstname list button ----------------------------------------
    $('#readByFirstName').on("click", function () {
        $.ajax({
            url: 'http://localhost:8000/users/fname',
            type: 'GET',
            dataType: 'json',

            success: function (data) {data.userList.sort(((a, b) => (a.firstName > b.firstName) ? 1 : (b.firstName > a.firstName) ? -1 : 0));
                 printTable(data.message, data.userList); },
            error: function (jqXHR) { printTable(jqXHR.responseJSON.message, []); }
        });
    });
    //--- click on the read list button ----------------------------------------
    $('#readByLastName').on("click", function () {
        $.ajax({
            url: 'http://localhost:8000/users/lname',
            type: 'GET',
            dataType: 'json',
            success: function (data) { printTable(data.message, data.userList); },
            error: function (jqXHR) { printTable(jqXHR.responseJSON.message, []); }
        });
    });
    //
    $('#readByRole').on("click", function () {
        $.ajax({
            url: 'http://localhost:8000/users/role',
            type: 'GET',
            dataType: 'json',
            success: function (data) { printTable(data.message, data.userList); },
            error: function (jqXHR) { printTable(jqXHR.responseJSON.message, []); }
        });
    });
});
//# sourceMappingURL=client.js.map