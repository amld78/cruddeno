import { Application, Router, send } from 'https://deno.land/x/oak/mod.ts';

import {
    Bson,
    MongoClient,
    ObjectId,
   
  } from "https://deno.land/x/mongo@v0.31.1/mod.ts";

  const client = new MongoClient();

  // Connecting to a Local Database
  await client.connect("mongodb://127.0.0.1:27017");  

// Defining schema interface
interface UserSchema {

    id: ObjectId;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  role: string;
  }
  


  const db = client.database("test");
  const users = db.collection<UserSchema>("userCollec");


const port = 8000;
const app = new Application();

const router = new Router();


app.use(router.allowedMethods());
app.use(router.routes());

app.addEventListener('listen', () => {
  console.log(`Listening on: localhost:${port}`);
});

//class user
class User {

  id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  role: string;

  constructor(id: number, fname: string, lname: string, email: string, pass: string, rolle: string) {
      this.id = id;
      this.firstName = fname;
      this.lastName = lname;
      this.email = email;
      this.password = pass;
      this.role = rolle;
  }
}

const ROOT_DIR = "./", ROOT_DIR_PATH = "/public";

app.use(async (ctx, next) => {
  if (!ctx.request.url.pathname.startsWith(ROOT_DIR_PATH)) {
    next();
    return;
  }
  const filePath = ctx.request.url.pathname.replace(ROOT_DIR_PATH, "");
  await send(ctx, filePath, {
    root: ROOT_DIR,
  });
});

//array of all users
let userList: User[] = [];
//addition of users
userList.push(new User(0, 'Semah', 'Derbali', "semder@gmail.com", 'samouha', 'Administrator'));
userList.push(new User(1, 'Abdelkader', 'Dhaw', "abdhaw@gmail.com", 'abdoughaddar', 'Coworker'));
userList.push(new User(2, 'Hamza', 'Ladab', "hamlad@gmail.com", 'hladab', 'Coworker'));
userList.push(new User(3, 'Alaya', 'Brigui', "alabri@gmail.com", '7awi', 'Client'));
userList.push(new User(4, 'Hichem', 'Aboucherrouen', "hicabo@gmail.com", 'hachoum', 'Client'));
userList.push(new User(5, 'Anis', 'Derbali', "zieder@gmail.com", 'zaydoun', 'Client'));

router.get('/', (ctx) => {
  let message: string;
  for (let users of userList) {
      if (users != null) {
          userList.sort(((a, b) => (a.firstName > b.firstName) ? 1 : (b.firstName > a.firstName) ? -1 : 0)); //Sort list by First Name
      }
  }
  message = "List sorted by First Name!";
  ctx.response.status = 200;
  ctx.response.body = {message: message, userList: userList};
});

//LIST BY ID
router.get("/users", (ctx) => {
    
  let message: string;
  for (let users of userList) {
      if (users != null) {
          userList.sort(((a, b) => (a.id > b.id) ? 1 : (b.id > a.id) ? -1 : 0)); //Sort list by ID
      }
  }
  message = "List sorted by ID!";
  ctx.response.status = 200;
  ctx.response.body = {message: message, userList: userList};
});

//LIST BY FIRST NAME
router.get("/users/fname", async (ctx) => {
console.log("hier f name");
   
let message = "User successfully added";
var  xusers = [] ;
const cursor = users.find();

// Skip & Limit
// cursor.skip(10).limit(10);

// iterate results
for await (const user of cursor) {
  xusers.push(user);
  console.log(user);
}
// xusers.sort(((a, b) => (a.firstName > b.firstName) ? 1 : (b.firstName > a.firstName) ? -1 : 0)); //Sort list by First Name
const count = await users.countDocuments({ } );
const getData = await users.find({});
console.log("Counter:"+count);
console.log(getData);
console.log(getData.toArray());
ctx.response.body = {message: message, userList: xusers, count: count};
ctx.response.status = 200;
});


/*let  xusers = [] ;
  addd
    const cursor = await users.find();
    const yusers = await cursor.toArray();
   // console.log(cursor);
    // iterate results
    for (const user of cursor) {
      xusers.push(user);
      console.log(user);  }
   let message = "List sorted by First Name!";
    ctx.response.status = 200;
    ctx.response.body = {message: message,  userList:xusers}; */

 /* let message: string;
  for (let users of userList) {
      if (users != null) {
          userList.sort(((a, b) => (a.firstName > b.firstName) ? 1 : (b.firstName > a.firstName) ? -1 : 0)); //Sort list by First Name
      }
  }
  message = "List sorted by First Name!";
  ctx.response.status = 200;
  ctx.response.body = {message: message, userList: userList}; 
});*/

//LIST BY LAST NAME
router.get("/users/lname", (ctx) => {
  let message: string;
  for (let users of userList) {
      if (users != null) {
          userList.sort(((a, b) => (a.lastName > b.lastName) ? 1 : (b.lastName > a.lastName) ? -1 : 0)); //Sort list by Last Name
      }
  }
  message = "List sorted by Last Name!";
  ctx.response.status = 200;
  ctx.response.body = {message: message, userList: userList};
});

//LIST BY ROLE
router.get("/users/role", (ctx) => {
  let message: string;
  for (let users of userList) {
      if (users != null) {
          userList.sort(((a, b) => (a.role > b.role) ? 1 : (b.role > a.role) ? -1 : 0)); //Sort list by Role
      }
  }
  message = "List sorted by Role!";
  ctx.response.status = 200;
  ctx.response.body = {message: message, userList: userList};
});

//SEARCH BY NAME OR ROLE
router.get("/users/:name", async (ctx) => {
  let input: string = ctx.params.name;
  let searchedList: User[] = [];
  let message = "Searched table";
var  xusers = [] ;
const cursor = users.find();


  for await (const user of cursor) {
    xusers.push(user);
    console.log(user);
  }
  
  const count = await users.countDocuments({ } );
  const getData = await users.find({_id:new ObjectId("63c83b33de6dd75555d643cc")});
  console.log("Counter:"+count);
  console.log(getData);
  console.log(getData.toArray());
  ctx.response.body = {message: message, userList: xusers, count: count};
  ctx.response.status = 200;
  });



  //go through our user list
 /* for (let users of userList) {
      if (users.firstName.substring(0, input.length).toUpperCase() === input.toUpperCase() || users.lastName.substring(0, input.length).toUpperCase() === input.toUpperCase()) {
          searchedList.push(users);
          //sort the searched list by first name
          searchedList.sort((a, b) => (a.firstName > b.firstName) ? 1 : (b.firstName > a.firstName) ? -1 : 0);
      } else if (users.role.substring(0, input.length).toUpperCase() === input.toUpperCase()) {
          searchedList.push(users);
          //sort the searched list by role
          searchedList.sort(((a, b) => (a.role > b.role) ? 1 : (b.role > a.role) ? -1 : 0));
      }
  }
  message = "found " + searchedList.length + " users";
  ctx.response.status = 200;
  ctx.response.body = {message: message, userList: userList}; 
}); */

// router.post('/', (ctx) => {
//   ctx.response.body = 'Received a POST HTTP method';
// });

// router.put('/', (ctx) => {
//   ctx.response.body = 'Received a PUT HTTP method';
// });

// Delete Routes

// router.delete("/user/:id", async (ctx) => {
  router.get("/userdel/:id", async (ctx) => {
  let id: number = ctx.params.id;
  let message: string = "";
  //check if ID is valid and if element in array not equal to null
 // if (!isNaN(id) && id >= 0 && id < userList.length && userList[id] != null) {
   //   if (id > -1) {
        const deleteCount: number = await users.deleteOne({
          _id: new Bson.ObjectId(id),
          
        //  userList.splice(id, 1); //id is the index to start with and 1 is the number of elements to be deleted
        //  message = userList[id].firstName + " " + userList[id].lastName + " deleted";
         });
         console.log(id);
          console.log(deleteCount);
        ctx.response.status = 200; 
        message = "wurde gelöscht";
 /* } else if (isNaN(id)) {
      message = "Id is not a number";
      ctx.response.status = 400;

  } else if (id < 0 || id > userList.length) {
      message = "Id out of range";
      ctx.response.status = 404;
  } else {
      message = "User already deleted";
      ctx.response.status = 410;
  } */
  ctx.response.body = {message: message, userList: userList};
});

//ADDD USER
router.post("/user",  async (ctx) => {
  //Requests also save empty Strings (use of ? req.body.firstName :"")
  //trim removes whitespaces from both ends of the String
  const { value } = ctx.request.body({ type: 'json' });
  const { firstName, lastName, email, password, role} = await value;
  let vorname: string = (firstName ? firstName : "").trim();
  let nachname: string = (lastName ? lastName : "").trim();
  let mail: string = (email ? email : "").trim();
  let pass: string = (password ? password : "").trim();
  let rolle: string = (role ? role : "").trim();
  let message: string = "";

  console.log("Hello world VOR");

  //if no blank field is left 
  if ((vorname !== "") && (nachname !== "") && (mail !== "") && (pass !== "") && (rolle !== "")) {
   // userList.push(new User(userList.length, vorname, nachname, mail, pass, rolle)); // add a new User

   console.log("Hello world ADD");
   
   const insertId = await users.insertOne({
   // id: "userList.length";
    firstName: vorname,
    lastName: nachname,
    email: mail,
    password: pass,
    role: rolle,
  });
 
  // console.log("Datenbank Inhalt"+getData.toArray());
 // getData.toArray().forEach((p) =>
 // {p.then((value) => {
  //  console.log(value);} ).catch((error) => {console.log(error);});})
    
   console.log("Hello world ADD"+insertId);
  console.log("Hello world NACH");

      message = "User successfully added";
      ctx.response.status =200;
  } else if (vorname == "") {
      ctx.response.status = 400;
      message = "Add first name ";
  } else if (nachname == "") {
      ctx.response.status = 400;
      message = "Add last name ";
  } else if (mail == "") {
      ctx.response.status = 400;
      message = "Add email address ";
  } else if (pass == "") {
      ctx.response.status = 400;
      message = "Add password ";
  } else if (rolle == "") {
      ctx.response.status = 400;
      message = "Add role ";
  }
  var  xusers = [] ;
  const cursor = users.find();

  // Skip & Limit
 // cursor.skip(10).limit(10);
  
  // iterate results
  for await (const user of cursor) {
    xusers.push(user);
    console.log(user);
  }

  const count = await users.countDocuments({ } );
 const getData = await users.find({});
  console.log("Counter:"+count);
  console.log(getData);
  console.log(getData.toArray());
  ctx.response.body = {message: message, userList: xusers,debug: xusers, count: count};
});

//EDIT METHOD
router.put("/user/:id", async (ctx) => {
  //Requests also save empty Strings (use of ? req.body.firstName :"")
  //trim removes whitespaces from both ends of the String
  const { value } = ctx.request.body({ type: 'json' });
  const { firstName, lastName, email, password, role} = await value;
  let vorname: string = (firstName ? firstName : "").trim();
  let nachname: string = (lastName ? lastName : "").trim();
  let mail: string = (email ? email : "").trim();
  let pass: string = (password ? password : "").trim();
  let rolle: string = (role ? role : "").trim();
  let message: string = "";
  let id: number = ctx.params.id; //id of user to be changed

  //check if ID is valid and if element in array not equal to null
  if (!isNaN(id) && id >= 0 && id < userList.length && userList[id] != null) {
      if (vorname !== "") {
          userList[id].firstName = vorname
      }
      if (nachname !== "") {
          userList[id].lastName = nachname
      }
      if (mail !== "") {
          userList[id].email = mail
      }
      if (pass !== "") {
          userList[id].password = pass
      }
      if (rolle !== "") {
          userList[id].role = rolle
      }
      message = "User successfully edited";
      ctx.response.status = 200;
  } else if (isNaN(id)) {
      message = "Id not a number";
      ctx.response.status = 400;
  } else if (id >= userList.length) {
      message = "Id out of index range";
      ctx.response.status = 400;
  } else {
        message = "User already deleted";
        ctx.response.status = 410;
  }
  ctx.response.body = {message: message, userList: userList};
});


await app.listen({ port });